#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>

#define PORT            5555
#define SIZE_OF_ARRAY(x)       (sizeof(x)/sizeof(x[0]))

int createSocket();
void connectToServer(int sock, char *hostname, uint16_t port);
void write_to_server (int fd, char* message);
char *get_server_responce(int sock);

int main(int argc, char **argv){
    if (argc < 2) {
        printf("First argument should be server IP/name!\n");
        return 1;
    }
    printf("server: %s\n", argv[1]);

    int sock = createSocket();
    connectToServer(sock, argv[1], PORT);

    /* Send data to the server. */

    write_to_server (sock, "version\n");
    printf("reply:\n%s\n", get_server_responce(sock));
    write_to_server (sock, "angles\n");
    printf("reply:\n%s\n", get_server_responce(sock));
    write_to_server (sock, "status\n");
    printf("reply:\n%s\n", get_server_responce(sock));
    write_to_server (sock, "set_angle lat=999\n");
    printf("reply:\n%s\n", get_server_responce(sock));
    write_to_server (sock, "set_angle lat=10\n");
    printf("reply:\n%s\n", get_server_responce(sock));
    write_to_server (sock, "set_angle lat=10\n");
    printf("reply:\n%s\n", get_server_responce(sock));
    write_to_server (sock, "status\n");
    printf("reply:\n%s\n", get_server_responce(sock));

    close (sock);
    exit (EXIT_SUCCESS);
}

/*
 * This is shared buffer for reply messages.
 * It can be shared because there can be only one thread doing the communication.
 * ... its still not pretty. But we are just testing.
 * */
static char read_buffer[500];

char *get_server_responce(int sock) {
    bzero(read_buffer,SIZE_OF_ARRAY(read_buffer));
    int nbytes = read (sock, read_buffer, SIZE_OF_ARRAY(read_buffer));
    if (nbytes < 0)
    {
        /* Read error. */
        perror ("read");
        exit (EXIT_FAILURE);
    }
    return read_buffer;
}

int createSocket() {
    int sock = socket (AF_INET, SOCK_STREAM, 0);
    if (sock < 0)
    {
        perror ("socket (client)");
        exit (EXIT_FAILURE);
    }
    return sock;
}

void connectToServer(int sock, char *hostname, uint16_t port) {
    struct sockaddr_in servername;
    struct hostent *hostinfo;

    servername.sin_family = AF_INET;
    servername.sin_port = htons (port);
    hostinfo = gethostbyname (hostname);
    if (hostinfo == NULL){
        fprintf (stderr, "Unknown host %s.\n", hostname);
        exit (EXIT_FAILURE);
    }
    servername.sin_addr = *(struct in_addr *) hostinfo->h_addr;

    if (0 > connect (sock,
                     (struct sockaddr *) &servername,
                     sizeof (servername))) {
        perror ("connect (client)");
        exit (EXIT_FAILURE);
    }
}

void write_to_server(int fd, char *message) {
    int nbytes;

    printf("sending:\n%s", message);
    nbytes = write (fd, message, strlen (message));
    if (nbytes < 0)
    {
        perror ("write");
        exit (EXIT_FAILURE);
    }
}
