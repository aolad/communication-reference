CFLAGS = -O2 -Wall $(INCLUDES)
LDFLAGS = -lm

TARGET      := server
SRCDIR      := src
INCDIR      := inc
BUILDDIR    := obj
TARGETDIR   := ./
OBJEXT      := o
SRCEXT      := c
INC         := -I$(INCDIR)
LIB         :=

SOURCES     := $(shell find $(SRCDIR) -type f -name "*.$(SRCEXT)")
OBJECTS     := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.$(OBJEXT)))

$(BUILDDIR)/%.$(OBJEXT): $(SRCDIR)/%.$(SRCEXT)
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<

all: directories $(TARGET) client

#Make the Directories
directories:
	@mkdir -p $(TARGETDIR)
	@mkdir -p $(BUILDDIR)

#Clean only Objects
clean:
	@$(RM) -rf $(BUILDDIR)

#Link
$(TARGET): $(OBJECTS)
	$(CC) -o $(TARGETDIR)/$(TARGET) $^ $(LIB)

#Compile
$(BUILDDIR)/%.$(OBJEXT): $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<

client: client.c
	$(CC) $(LDFLAGS) -o $@ $<

run: all
	./server 5 &
	./client localhost


#Non-File Targets
.PHONY: all clean run directories