#ifndef TEMPLINET_COMMUNICATION_REFERENCE_STATE_H
#define TEMPLINET_COMMUNICATION_REFERENCE_STATE_H

int get_lat();
int get_long();
int get_back();
int get_calf();
int get_thigh();
const char *get_version();
int get_status(char* buffer, int buffer_size);
int get_moving();
int set_moving(char* what, int value);

#endif //TEMPLINET_COMMUNICATION_REFERENCE_STATE_H
