#include <poll.h>
#include <string.h>
#include <arpa/inet.h>
#include <time.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include "cmds.h"
#include "state.h"

int execute_angles(char* buffer, int buffer_size);
int execute_version(char* buffer, int buffer_size);
int execute_status(char* buffer, int buffer_size);
int execute_set_angle(char* in_buffer, char* out_buffer, int buffer_size);

int execute_cmd(char* in_buffer, char* out_buffer, int in_buffer_size, int out_buffer_size) {

    if (strncmp(in_buffer, "angles", sizeof("angles") - 1) == 0) return execute_angles(out_buffer, out_buffer_size);
    if (strncmp(in_buffer, "version", sizeof ("version")-1) == 0) return execute_version(out_buffer,out_buffer_size);
    if (strncmp(in_buffer, "status", sizeof ("status")-1) == 0) return execute_status(out_buffer,out_buffer_size);
    if (strncmp(in_buffer, "set_angle", sizeof ("set_angle")-1) == 0) return execute_set_angle(in_buffer,out_buffer,out_buffer_size);
    return 0;
}
int execute_angles(char* buffer, int buffer_size){
    return sprintf(buffer,"angles lat=%d long=%d back=%d calf=%d thigh=%d\n", get_lat(), get_long(), get_back(), get_calf(), get_thigh());
}

int execute_version(char* buffer, int buffer_size){
    return sprintf(buffer,"version %s\n", get_version());
}

int execute_status(char* buffer, int buffer_size){
    return get_status(buffer, buffer_size);
}

int split_into_pair(char* buffer){
    int ret, i;
    for (i = 0; i < 30; ++i) {
        if (buffer[i] == '=')break;
    }
    sscanf(buffer+i+1,"%d", &ret);
    for (; i < 30; ++i) {
        buffer[i] = 0;
    }
    return ret;
}

int execute_set_angle(char* in_buffer, char* out_buffer, int buffer_size){
    char what_to_set[30];
    int value;

    sscanf(in_buffer,"set_angle %s", what_to_set);
    value = split_into_pair(what_to_set);

    //printf("setting %s=%d\n", what_to_set, value);

    if (get_moving()){
        return sprintf(out_buffer,"set_angle %s=%d ack=0 reason=busy\n", what_to_set, value);
    }

    if (set_moving(what_to_set, value) == 0){
        return sprintf(out_buffer,"set_angle %s=%d ack=1\n", what_to_set, value);
    }else{
        return sprintf(out_buffer,"set_angle %s=%d ack=0 reason=invalid\n", what_to_set, value);
    }
}
