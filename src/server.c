#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>
#include <arpa/inet.h>
#include <string.h>
#include <poll.h>

#include "cmds.h"

#define PORT    5555
#define MAXMSG  512


static int tick(){
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC,&ts);
    return ts.tv_sec;
}


#define BUFFER_SIZE 1024
#define on_error(...)                 \
    {                                 \
        fprintf(stderr, __VA_ARGS__); \
        perror("cause by");           \
        fflush(stderr);               \
        exit(1);                      \
    }

void communicate(int client_fd){
    char buff_in[BUFFER_SIZE];
    char buff_out[BUFFER_SIZE];

    bzero(buff_in, BUFFER_SIZE);

    while(1){
        int read_bytes = recv(client_fd, buff_in, BUFFER_SIZE-1, 0);
        if (read_bytes <= 0)
        {
            break;
        }

        execute_cmd(buff_in, buff_out, BUFFER_SIZE, BUFFER_SIZE);

        if (send(client_fd, buff_out, strlen(buff_out), 0) < 0)
        {
            break;
        }
        bzero(buff_in, read_bytes);
        bzero(buff_out, strlen(buff_out));
    }
    printf("Connection closed: %d\n", client_fd);
    close(client_fd);
}




int main(int argc, char *argv[]){

    int lifespan = 0;
    if (argc > 1) {
        if ( sscanf(argv[1], "%d", &lifespan) != 1 ){
            perror("First argument should be empty or number (runtime duration in sec)");
            return 1;
        }
    }

    /* spawn a pthread used for communication */


    int end = 0;
    int start = tick();

    int server_fd, client_fd;                  /* server and client file descriptor*/
    struct sockaddr_in serveraddr, clientaddr; /* server and client address*/
    socklen_t client_len = sizeof(clientaddr); /* byte size of client's address */

    server_fd = socket(PF_INET, SOCK_STREAM, 0);
    if (server_fd < 0){
        return 1;
    }

    bzero((char *)&serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;                /* this is an IPv4 Internet address */
    serveraddr.sin_port = htons(PORT);              /* this is the port we will listen on */
    serveraddr.sin_addr.s_addr = htonl(INADDR_ANY); /* let the system figure out our IP address */


    if (bind(server_fd, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) < 0){
        return 1;
    }

    /* only one connection is expected */
    if (listen(server_fd, 1) < 0) {
        return 1;
    }

    struct pollfd server_fd_poll;
    server_fd_poll.fd = server_fd;
    server_fd_poll.events = POLLIN;

    while (end == 0)
    {

        if ( poll(&server_fd_poll, 1, 1000) > 0){
            client_fd = accept(server_fd, (struct sockaddr *)&clientaddr, &client_len);
            if (client_fd < 0) {
                return 1;
            }
            communicate(client_fd);
        }

        if ((lifespan > 0) && (tick() > start+lifespan) ) end = 1;
    }

    return 0;
}