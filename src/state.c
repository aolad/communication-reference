#include "state.h"

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

int get_lat(){
    return rand()%90;
}

int get_long(){
    return rand()%90;
}

int get_back(){
    return rand()%90;
}

int get_calf(){
    return rand()%90;
}

int get_thigh(){
    return rand()%90;
}

const char *get_version(){
    return "0.0.1";
}

bool moving = false;
int last_move_cmd = 0;
char errors[1024];

int get_status(char* buffer, int buffer_size){
    int written = 0;
    if (moving){
        written += sprintf(buffer, "status moving\n");
    }else{
        written += sprintf(buffer, "status ready\n");
    }
    if (errors[0] != 0){
        written += sprintf(buffer+written, "error messages:\n%s", errors);
        bzero(errors, sizeof(errors));
    }
    return written;
}

static int tick(){
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC,&ts);
    return ts.tv_sec;
}

int get_moving(){
    if (tick() > last_move_cmd+5 ) moving = false;
    return moving;
}

int set_moving(char* what, int value){
    if (tick() > last_move_cmd+5 ) moving = false;
    if (moving) {
        return 1;
    }
    if (value > 360) return 2;
    last_move_cmd = tick();
    moving = true;
    /* just to show some error message */
    sprintf(errors,"SOME RANDOM ERROR MESSAGE - SOMETHING IS WRONG WITH THE MOTOR");
    return 0;
}