#ifndef TEMPLINET_COMMUNICATION_REFERENCE_CMDS_H
#define TEMPLINET_COMMUNICATION_REFERENCE_CMDS_H

int execute_cmd(char* in_buffer, char* out_buffer, int in_buffer_size, int out_buffer_size);

#endif //TEMPLINET_COMMUNICATION_REFERENCE_CMDS_H
